<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class InstallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
        ]);
        DB::table('services')->insert([
            'name' => 'Gas',
            'name_ar' => 'غاز',
            'size' => '12',
            'image' => '/',
            'price' => 60,
        ]);
        DB::table('subscriptions')->insert([
            'name' => 'First Package',
            'name_ar' => 'الحزمة الاولى',
            'description' => 'First Package',
            'description_ar' => 'الحزمة الاولى',
            'price' => 299,
            'image' => '/',
            'months_period' => 12,
        ]);
        DB::table('time_intervals')->insert([
            'name' => '10 - 12 AM',
            'name_ar' => '10 - 12 ص',
        ]);
        DB::table('settings')->insert([
            'key' => 'about',
            'name' => 'About Us',
            'name_ar' => 'من نحن',
            'value' => 'About Us',
            'value_ar' => 'من نحن',
        ]);
        DB::table('settings')->insert([
            'key' => 'privacy',
            'name' => 'Privacy',
            'name_ar' => 'سياسة الخصوصية',
            'value' => 'Privacy',
            'value_ar' => 'سياسة الخصوصية',
        ]);
    }
}
