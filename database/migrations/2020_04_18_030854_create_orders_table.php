<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('provider_id');
            $table->unsignedBigInteger('time_interval_id');
            $table->float('price');
            $table->string('address')->nullable();
            $table->string('customer_lat')->nullable();
            $table->string('customer_lng')->nullable();
            $table->string('provider_lat')->nullable();
            $table->string('provider_lng')->nullable();
            $table->timestamp('deliver_date')->nullable();
            $table->tinyInteger('status')->default(\App\Helpers\Constant::ORDER_STATUS['Pending']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
