<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

/**
 * @property integer id
 * @property string name
 * @property string name_ar
 * @property string description
 * @property string description_ar
 * @property float price
 * @property string image
 * @property integer months_period
 */
class Subscription extends Model
{
    protected $table = 'subscriptions';
    protected $fillable = ['name','description','name_ar','description_ar','price','image','months_period'];
    protected static function boot() {
        parent::boot();
        static::deleting(function($Object) {
            $VendorSubscription = $Object->vendor_subscriptions->pluck('id');
            VendorSubscription::destroy($VendorSubscription);
        });
    }
    public function vendor_subscriptions(){
        return $this->hasMany(VendorSubscription::class,'subscription_id','id');
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getNameAr(): string
    {
        return $this->name_ar;
    }

    /**
     * @param string $name_ar
     */
    public function setNameAr(string $name_ar): void
    {
        $this->name_ar = $name_ar;
    }

    /**
     * @return string
     */
    public function getDescriptionAr(): string
    {
        return $this->description_ar;
    }

    /**
     * @param string $description_ar
     */
    public function setDescriptionAr(string $description_ar): void
    {
        $this->description_ar = $description_ar;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getMonthsPeriod(): int
    {
        return $this->months_period;
    }

    /**
     * @param int $months_period
     */
    public function setMonthsPeriod(int $months_period): void
    {
        $this->months_period = $months_period;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @param $value
     */
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $destination_path = "storage/subscriptions/";
        $request = Request::instance();
        $attribute_value = $value;
        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name)) {
            $file = $request->file($attribute_name);
            if ($file->isValid()) {
                $file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
                $file->move($destination_path, $file_name);
                $attribute_value =  $destination_path.$file_name;
            }
        }
        $this->attributes[$attribute_name] = $attribute_value;
    }


}
