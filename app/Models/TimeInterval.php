<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property string name
 * @property string name_ar
 */
class TimeInterval extends Model
{
    protected $table = 'time_intervals';
    protected $fillable = ['name','name_ar'];

    /**
     *
     */
    protected static function boot() {
        parent::boot();
        static::deleting(function($Object) {
            $Order = $Object->orders->pluck('id');
            Order::destroy($Order);
        });
    }
    public function orders(){
        return $this->hasMany(Order::class,'time_interval_id','id');
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNameAr(): string
    {
        return $this->name_ar;
    }

    /**
     * @param string $name_ar
     */
    public function setNameAr(string $name_ar): void
    {
        $this->name_ar = $name_ar;
    }

}
