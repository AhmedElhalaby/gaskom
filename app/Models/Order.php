<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Request;

/**
 * @property integer id
 * @property integer user_id
 * @property integer provider_id
 * @property integer time_interval_id
 * @property float price
 * @property string|null address
 * @property mixed deliver_date
 * @property mixed customer_lat
 * @property mixed customer_lng
 * @property mixed provider_lat
 * @property mixed provider_lng
 * @property mixed status
 * @property boolean is_reviewed
 * @method Order find(int $id)
 */
class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['user_id','provider_id','time_interval_id','price','address','deliver_at','customer_lat','customer_lng','provider_lat','provider_lng','status'];

    protected static function boot() {
        parent::boot();
        static::deleting(function($Object) {
            if($Object->review){
                $Object->review->delete();
            }
            $OrderServices = $Object->order_services->pluck('id');
            OrderService::destroy($OrderServices);
        });
    }
    /**
     * @return BelongsTo
     */
    public function customer(){
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * @return BelongsTo
     */
    public function provider(){
        return $this->belongsTo(User::class,'provider_id');
    }

    /**
     * @return BelongsTo
     */
    public function order(){
        return $this->belongsTo(Order::class);
    }
    /**
     * @return BelongsTo
     */
    public function time_interval(){
        return $this->belongsTo(TimeInterval::class);
    }

    /**
     * @return HasMany
     */
    public function order_services(){
        return $this->hasMany(OrderService::class);
    }

    public function review(){
        return $this->hasOne(Review::class,'order_id','id');
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getProviderId(): int
    {
        return $this->provider_id;
    }

    /**
     * @param int $provider_id
     */
    public function setProviderId(int $provider_id): void
    {
        $this->provider_id = $provider_id;
    }

    /**
     * @return int
     */
    public function getTimeIntervalId(): int
    {
        return $this->time_interval_id;
    }

    /**
     * @param int $time_interval_id
     */
    public function setTimeIntervalId(int $time_interval_id): void
    {
        $this->time_interval_id = $time_interval_id;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getDeliverDate()
    {
        return $this->deliver_date;
    }

    /**
     * @param mixed $deliver_date
     */
    public function setDeliverDate($deliver_date): void
    {
        $this->deliver_date = $deliver_date;
    }

    /**
     * @return mixed
     */
    public function getCustomerLat()
    {
        return $this->customer_lat;
    }

    /**
     * @param mixed $customer_lat
     */
    public function setCustomerLat($customer_lat): void
    {
        $this->customer_lat = $customer_lat;
    }

    /**
     * @return mixed
     */
    public function getCustomerLng()
    {
        return $this->customer_lng;
    }

    /**
     * @param mixed $customer_lng
     */
    public function setCustomerLng($customer_lng): void
    {
        $this->customer_lng = $customer_lng;
    }

    /**
     * @return mixed
     */
    public function getProviderLat()
    {
        return $this->provider_lat;
    }

    /**
     * @param mixed $provider_lat
     */
    public function setProviderLat($provider_lat): void
    {
        $this->provider_lat = $provider_lat;
    }

    /**
     * @return mixed
     */
    public function getProviderLng()
    {
        return $this->provider_lng;
    }

    /**
     * @param mixed $provider_lng
     */
    public function setProviderLng($provider_lng): void
    {
        $this->provider_lng = $provider_lng;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isIsReviewed(): bool
    {
        return $this->is_reviewed;
    }

    /**
     * @param bool $is_reviewed
     */
    public function setIsReviewed(bool $is_reviewed): void
    {
        $this->is_reviewed = $is_reviewed;
    }

}
