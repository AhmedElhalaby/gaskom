<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Request;

/**
 * @property integer id
 * @property string name
 * @property string name_ar
 * @property string size
 * @property string image
 * @property float price
 */
class Service extends Model
{
    protected $table = 'services';
    protected $fillable = ['name','name_ar','size','image','price'];

    /**
     *
     */
    protected static function boot() {
        parent::boot();
        static::deleting(function($Object) {
            $VendorServices = $Object->vendor_services->pluck('id');
            VendorService::destroy($VendorServices);
            $OrderServices = $Object->order_services->pluck('order_id');
            Order::destroy($OrderServices);
        });
    }

    /**
     * @return HasMany
     */
    public function vendor_services(){
        return $this->hasMany(VendorService::class,'service_id','id');
    }
    /**
     * @return HasMany
     */
    public function order_services(){
        return $this->hasMany(OrderService::class,'service_id','id');
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNameAr(): string
    {
        return $this->name_ar;
    }

    /**
     * @param string $name_ar
     */
    public function setNameAr(string $name_ar): void
    {
        $this->name_ar = $name_ar;
    }

    /**
     * @return string
     */
    public function getSize(): string
    {
        return $this->size;
    }

    /**
     * @param string $size
     */
    public function setSize(string $size): void
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param $image
     */
    public function setImage($image): void
    {
        $this->price = $image;
    }

    /**
     * @param $value
     */
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $destination_path = "storage/services/";
        $request = Request::instance();
        $attribute_value = $value;
        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name)) {
            $file = $request->file($attribute_name);
            if ($file->isValid()) {
                $file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
                $file->move($destination_path, $file_name);
                $attribute_value =  $destination_path.$file_name;
            }
        }
        $this->attributes[$attribute_name] = $attribute_value;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

}
