<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer user_id
 * @property integer subscription_id
 * @property mixed expire_at
 * @property integer payment_type
 * @property string|null payment_document
 * @property string|null payment_token
 * @property boolean status
 */
class VendorSubscription extends Model
{
    protected $table = 'vendor_subscriptions';
    protected $fillable = ['user_id','subscription_id','expire_at','status','payment_type','payment_document','payment_token'];

    public function subscription(){
        return $this->belongsTo(Subscription::class,'subscription_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getSubscriptionId(): int
    {
        return $this->subscription_id;
    }

    /**
     * @param int $subscription_id
     */
    public function setSubscriptionId(int $subscription_id): void
    {
        $this->subscription_id = $subscription_id;
    }

    /**
     * @return mixed
     */
    public function getExpireAt()
    {
        return $this->expire_at;
    }

    /**
     * @param mixed $expire_at
     */
    public function setExpireAt($expire_at): void
    {
        $this->expire_at = $expire_at;
    }

    /**
     * @return int
     */
    public function getPaymentType(): int
    {
        return $this->payment_type;
    }

    /**
     * @param int $payment_type
     */
    public function setPaymentType(int $payment_type): void
    {
        $this->payment_type = $payment_type;
    }

    /**
     * @return string|null
     */
    public function getPaymentDocument(): ?string
    {
        return $this->payment_document;
    }

    /**
     * @param mixed $payment_document
     */
    public function setPaymentDocument($payment_document): void
    {
        $this->payment_document = $payment_document;
    }

    /**
     * @return string|null
     */
    public function getPaymentToken(): ?string
    {
        return $this->payment_token;
    }

    /**
     * @param string|null $payment_token
     */
    public function setPaymentToken(?string $payment_token): void
    {
        $this->payment_token = $payment_token;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isExpire(){
        if(now() < $this->getExpireAt()){
            return true;
        }
        return false;
    }
}
