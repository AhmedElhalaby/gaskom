<?php

namespace App\Traits;

trait RandomUniqueKey
{
    /**
     * Boot function from laravel.
     */
    public static function bootRandomUniqueKey()
    {
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = $model->generateBarcodeNumber();
        });
    }

    function generateBarcodeNumber() {
        $number = mt_rand(100000000, 999999999); // better than rand()

        // call the same function if the barcode exists already
        if ($this->barcodeNumberExists($number)) {
            return $this->generateBarcodeNumber();
        }

        // otherwise, it's valid and can be used
        return $number;
    }

    function barcodeNumberExists($number) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return get_class($this)::where($this->getKeyName(),$number)->exists();
    }
}