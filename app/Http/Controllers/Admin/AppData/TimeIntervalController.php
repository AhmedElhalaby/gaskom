<?php

namespace App\Http\Controllers\Admin\AppData;

use App\Http\Controllers\Admin\Controller;
use App\Models\TimeInterval;
use App\Traits\AhmedPanelTrait;
use Carbon\Carbon;

class TimeIntervalController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $this->setRedirect('admin/app_data/time_intervals');
        $this->setEntity(new TimeInterval());
        $this->setTable('time_intervals');
        $this->setLang('TimeInterval');
        $this->setColumns([
            'name'=> [
                'name'=>'name',
                'type'=>'text-custom',
                'custom'=>function ($object){
                    return Carbon::parse($object->name)->format('H:i A') . ' - '.Carbon::parse($object->name_ar)->format('H:i A');
                },
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->setFields([
            'name'=> [
                'name'=>'name',
                'type'=>'time',
                'is_required'=>true
            ],
            'name_ar'=> [
                'name'=>'name_ar',
                'type'=>'time',
                'is_required'=>true
            ],
        ]);
        $this->SetLinks([
            'edit',
            'delete',
        ]);
    }

}
