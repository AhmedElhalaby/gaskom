<?php

namespace App\Http\Controllers\Admin\VendorManagement;

use App\Helpers\Constant;
use App\Http\Controllers\Admin\Controller;
use App\Models\User;
use App\Traits\AhmedPanelTrait;

class VendorController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $Objects = new User();
        $Objects = $Objects->where('type',Constant::USER_TYPE['Vendor']);
        $this->setRedirect('admin/vendor_management/vendors');
        $this->setEntity($Objects);
        $this->setTable('users');
        $this->setLang('Vendor');
        $this->setExport(false);
        $this->setCreate(false);
        $this->setViewShow('admin.vendor_management.vendors.show');
        $this->setColumns([
            'image'=>[
                'name'=>'image',
                'type'=>'image',
                'is_searchable'=>false,
                'order'=>false
            ],
            'name'=> [
                'name'=>'name',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'mobile'=> [
                'name'=>'mobile',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'address'=> [
                'name'=>'address',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'license_plate'=> [
                'name'=>'license_plate',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'is_active'=> [
                'name'=>'is_active',
                'type'=>'active',
                'is_searchable'=>true,
                'order'=>true
            ],
            'created_at'=> [
                'name'=>'created_at',
                'type'=>'datetime',
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->SetLinks([
            'show',
            'active',
            'change_password',
            'delete',
        ]);
    }
}
