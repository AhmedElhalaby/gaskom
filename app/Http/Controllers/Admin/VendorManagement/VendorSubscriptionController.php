<?php

namespace App\Http\Controllers\Admin\VendorManagement;

use App\Helpers\Constant;
use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\VendorSubscription\ApproveRequest;
use App\Http\Requests\Admin\VendorSubscription\DeclineRequest;
use App\Models\Subscription;
use App\Models\User;
use App\Models\VendorSubscription;
use App\Traits\AhmedPanelTrait;

class VendorSubscriptionController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $this->setRedirect('admin/vendor_management/subscriptions');
        $this->setEntity(new VendorSubscription());
        $this->setTable('vendor_subscriptions');
        $this->setLang('VendorSubscription');
        $this->setExport(false);
        $this->setCreate(false);
        $this->setColumns([
            'user_id'=>[
                'name'=>'user_id',
                'type'=>'relation',
                'relation'=>[
                    'data'=> User::where('type',Constant::USER_TYPE['Vendor'])->get(),
                    'name'=>'name',
                    'entity'=>'user'
                ],
                'is_searchable'=>true,
                'order'=>false
            ],
            'subscription_id'=>[
                'name'=>'subscription_id',
                'type'=>'relation',
                'relation'=>[
                    'data'=> Subscription::all(),
                    'name'=>(app()->getLocale()=='ar')?'name_ar':'name',
                    'entity'=>'subscription'
                ],
                'is_searchable'=>true,
                'order'=>false
            ],
            'expire_at'=> [
                'name'=>'expire_at',
                'type'=>'date',
                'is_searchable'=>true,
                'order'=>true
            ],
            'payment_type'=> [
                'name'=>'payment_type',
                'type'=>'select',
                'data'=>[
                    Constant::PAYMENT_TYPE['Cash']=>__('crud.VendorSubscription.payment_types.Cash'),
                    Constant::PAYMENT_TYPE['Bank']=>__('crud.VendorSubscription.payment_types.Bank'),
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
            'payment_document'=> [
                'name'=>'payment_document',
                'type'=>'file',
                'is_searchable'=>true,
                'order'=>true
            ],
            'status'=> [
                'name'=>'status',
                'type'=>'select',
                'data'=>[
                    Constant::VENDOR_SUBSCRIPTION_STATUS['Pending']=>__('crud.VendorSubscription.statuses.Pending'),
                    Constant::VENDOR_SUBSCRIPTION_STATUS['Approved']=>__('crud.VendorSubscription.statuses.Approved'),
                    Constant::VENDOR_SUBSCRIPTION_STATUS['Declined']=>__('crud.VendorSubscription.statuses.Declined'),
                ],
                'is_searchable'=>true,
                'order'=>true
            ],

        ]);
        $this->SetLinks([
            'approve'=>[
                'route'=>'approve',
                'icon'=>'fa-check-square',
                'lang'=>__('crud.VendorSubscription.links.approve'),
                'condition'=>function ($Object){
                    return ($Object->status == Constant::VENDOR_SUBSCRIPTION_STATUS['Pending']);
                }
            ],
            'decline'=>[
                'route'=>'decline',
                'icon'=>'fa-window-close',
                'lang'=>__('crud.VendorSubscription.links.decline'),
                'condition'=>function ($Object){
                    return ($Object->status == Constant::VENDOR_SUBSCRIPTION_STATUS['Pending']);
                }
            ],
        ]);
    }

    public function approve(ApproveRequest $request,$id){
        return $request->preset($this->getRedirect(),$id);
    }
    public function decline(DeclineRequest $request,$id){
        return $request->preset($this->getRedirect(),$id);
    }
}
