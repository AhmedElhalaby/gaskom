<?php

namespace App\Http\Controllers\Admin\CustomerManagement;

use App\Helpers\Constant;
use App\Http\Controllers\Admin\Controller;
use App\Models\User;
use App\Traits\AhmedPanelTrait;

class CustomerController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $Objects = new User();
        $Objects = $Objects->where('type',Constant::USER_TYPE['Customer']);
        $this->setRedirect('admin/customer_management/customers');
        $this->setEntity($Objects);
        $this->setTable('users');
        $this->setLang('Customer');
        $this->setExport(false);
        $this->setColumns([
            'name'=> [
                'name'=>'name',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'mobile'=> [
                'name'=>'mobile',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'address'=> [
                'name'=>'address',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'is_active'=> [
                'name'=>'is_active',
                'type'=>'active',
                'is_searchable'=>true,
                'order'=>true
            ],
            'created_at'=> [
                'name'=>'created_at',
                'type'=>'datetime',
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->setFields([
            'name'=> [
                'name'=>'name',
                'type'=>'text',
                'is_required'=>true
            ],
            'mobile'=> [
                'name'=>'mobile',
                'type'=>'text',
                'is_required'=>true,
                'is_unique'=>true
            ],
            'address'=> [
                'name'=>'address',
                'type'=>'text',
                'is_required'=>false,
            ],
            'password'=> [
                'name'=>'password',
                'type'=>'password',
                'confirmation'=>true,
                'editable'=>false,
                'is_required'=>true,
                'export'=>false
            ],
            'is_active'=> [
                'name'=>'is_active',
                'type'=>'active',
                'is_required'=>true
            ],
        ]);
        $this->SetLinks([
            'edit',
            'active',
            'change_password',
        ]);
    }
}
