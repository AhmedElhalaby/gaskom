<?php

namespace App\Http\Controllers\Admin\OrderManagement;

use App\Helpers\Constant;
use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\VendorSubscription\ApproveRequest;
use App\Http\Requests\Admin\VendorSubscription\DeclineRequest;
use App\Models\Order;
use App\Models\Subscription;
use App\Models\TimeInterval;
use App\Models\User;
use App\Models\VendorSubscription;
use App\Traits\AhmedPanelTrait;
use Carbon\Carbon;

class OrderController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $this->setRedirect('admin/order_management/orders');
        $this->setEntity(new Order());
        $this->setTable('orders');
        $this->setLang('Order');
        $this->setExport(false);
        $this->setCreate(false);
        $this->setColumns([
            'user_id'=>[
                'name'=>'user_id',
                'type'=>'relation',
                'relation'=>[
                    'data'=> User::where('type',Constant::USER_TYPE['Customer'])->get(),
                    'name'=>'name',
                    'entity'=>'customer'
                ],
                'is_searchable'=>true,
                'order'=>false
            ],
            'provider_id'=>[
                'name'=>'provider_id',
                'type'=>'relation',
                'relation'=>[
                    'data'=> User::where('type',Constant::USER_TYPE['Vendor'])->get(),
                    'name'=>'name',
                    'entity'=>'provider'
                ],
                'is_searchable'=>true,
                'order'=>false
            ],
            'time_interval_id'=>[
                'name'=>'time_interval_id',
                'type'=>'text-custom',
                'custom'=>function ($object){
                    return ($object->time_interval)?Carbon::parse($object->time_interval->name)->format('H:i A') . ' - '.Carbon::parse($object->time_interval->name_ar)->format('H:i A'):' - ';
                },
                'is_searchable'=>true,
                'order'=>false
            ],
            'price'=> [
                'name'=>'price',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'status'=> [
                'name'=>'status',
                'type'=>'select',
                'data'=>[
                    Constant::ORDER_STATUS['Pending']=>__('crud.Order.OrderStatus.Pending'),
                    Constant::ORDER_STATUS['Canceled']=>__('crud.Order.OrderStatus.Canceled'),
                    Constant::ORDER_STATUS['Accepted']=>__('crud.Order.OrderStatus.Accepted'),
                    Constant::ORDER_STATUS['Rejected']=>__('crud.Order.OrderStatus.Rejected'),
                    Constant::ORDER_STATUS['Processing']=>__('crud.Order.OrderStatus.Processing'),
                    Constant::ORDER_STATUS['Delivered']=>__('crud.Order.OrderStatus.Delivered'),
                    Constant::ORDER_STATUS['Received']=>__('crud.Order.OrderStatus.Received'),
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
            'created_at'=> [
                'name'=>'created_at',
                'type'=>'datetime',
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->SetLinks([

        ]);
    }

}
