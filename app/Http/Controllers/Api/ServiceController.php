<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Service\MeRequest;
use App\Http\Requests\Api\Service\UpdateRequest;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class ServiceController extends Controller
{
    use ResponseTrait;

    /**
     * @param MeRequest $request
     * @return JsonResponse
     */
    public function me(MeRequest $request){
        return $request->persist();
    }

    /**
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    public function update(UpdateRequest $request){
        return $request->persist();
    }
}
