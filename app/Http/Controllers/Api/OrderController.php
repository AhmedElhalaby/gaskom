<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Order\CancelRequest;
use App\Http\Requests\Api\Order\ChangeStatusRequest;
use App\Http\Requests\Api\Order\ProviderRequest;
use App\Http\Requests\Api\Order\ListRequest;
use App\Http\Requests\Api\Order\ShowRequest;
use App\Http\Requests\Api\Order\StoreRequest;
use App\Http\Requests\Api\Order\ToggleFavouriteRequest;
use App\Http\Requests\Api\Order\ReviewRequest;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    use ResponseTrait;

    /**
     * @param ProviderRequest $request
     * @return JsonResponse
     */
    public function providers(ProviderRequest $request){
        return $request->persist();
    }

    /**
     * @param ListRequest $request
     * @return JsonResponse
     */
    public function list(ListRequest $request){
        return $request->persist();
    }
    /**
     * @param ShowRequest $request
     * @return JsonResponse
     */
    public function show(ShowRequest $request){
        return $request->persist();
    }

    /**
     * @param CancelRequest $request
     * @return JsonResponse
     */
    public function cancel(CancelRequest $request){
        return $request->persist();
    }

    /**
     * @param ChangeStatusRequest $request
     * @return JsonResponse
     */
    public function change_status(ChangeStatusRequest $request){
        return $request->persist();
    }

    /**
     * @param ReviewRequest $request
     * @return JsonResponse
     */
    public function review(ReviewRequest $request){
        return $request->persist();
    }

    /**
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request){
        return $request->persist();
    }
}
