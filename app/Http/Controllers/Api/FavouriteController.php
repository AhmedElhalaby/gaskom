<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Favourite\ListRequest;
use App\Http\Requests\Api\Favourite\ToggleRequest;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class FavouriteController extends Controller
{
    use ResponseTrait;

    /**
     * @param ListRequest $request
     * @return JsonResponse
     */
    public function list(ListRequest $request){
        return $request->persist();
    }

    /**
     * @param ToggleRequest $request
     * @return JsonResponse
     */
    public function toggle(ToggleRequest $request){
        return $request->persist();
    }
}
