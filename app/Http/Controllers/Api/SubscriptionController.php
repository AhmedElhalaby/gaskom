<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Constant;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Subscription\MeRequest;
use App\Http\Requests\Api\Subscription\SubscribeRequest;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class SubscriptionController extends Controller
{
    use ResponseTrait;

    /**
     * @param MeRequest $request
     * @return JsonResponse
     */
    public function me(MeRequest $request){
        return $request->persist();
    }

    /**
     * @param SubscribeRequest $request
     * @return JsonResponse
     */
    public function subscribe(SubscribeRequest $request){
        return $request->persist();
    }
}
