<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AddressBook\IndexRequest;
use App\Http\Requests\Api\AddressBook\StoreRequest;
use App\Http\Requests\Api\AddressBook\UpdateRequest;
use App\Http\Requests\Api\AddressBook\ShowRequest;
use App\Http\Requests\Api\AddressBook\DestroyRequest;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class AddressBookController extends Controller
{
    use ResponseTrait;

    /**
     * @param IndexRequest $request
     * @return JsonResponse
     */
    public function index(IndexRequest $request){
        return $request->persist();
    }

    /**
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request){
        return $request->persist();
    }

    /**
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    public function update(UpdateRequest $request){
        return $request->persist();
    }

    /**
     * @param ShowRequest $request
     * @return JsonResponse
     */
    public function show(ShowRequest $request){
        return $request->persist();
    }
    /**
     * @param DestroyRequest $request
     * @return JsonResponse
     */
    public function destroy(DestroyRequest $request){
        return $request->persist();
    }

}
