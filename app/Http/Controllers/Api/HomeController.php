<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Constant;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\BankAccountResource;
use App\Http\Resources\Api\ServiceResource;
use App\Http\Resources\Api\SubscriptionResource;
use App\Http\Resources\Api\TimeIntervalResource;
use App\Models\BankAccount;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Subscription;
use App\Models\TimeInterval;
use App\Traits\ResponseTrait;

class HomeController extends Controller
{
    use ResponseTrait;
    public function install(){
        $data = [];
        $data['Services'] = ServiceResource::collection(Service::all());
        $data['Subscriptions'] = SubscriptionResource::collection(Subscription::all());
        $data['TimeIntervals'] = TimeIntervalResource::collection(TimeInterval::all());
        $data['Settings'] = Setting::pluck((app()->getLocale() =='en')?'value':'value_ar','key')->toArray();
        $data['UserType'] = Constant::USER_TYPE;
        $data['PaymentType'] = Constant::PAYMENT_TYPE;
        $data['OrderStatus'] = Constant::ORDER_STATUS;
        $data['NotificationType'] = Constant::NOTIFICATION_TYPE;
        $data['BankAccounts'] = BankAccountResource::collection(BankAccount::all());
        return $this->successJsonResponse([],$data,'data');
    }
}
