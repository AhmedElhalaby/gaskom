<?php

namespace App\Http\Resources\Api;

use App\Models\Favourite;
use App\Models\Service;
use App\Models\VendorService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AddressBookResource extends JsonResource
{
    protected $token;

    /**
     * ExportResource constructor.
     * @param $resource
     * @param array $token
     */
    public function __construct($resource, $token =null)
    {
        $this->token = $token;
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['address'] = $this->getAddress();
        $Object['lat'] = $this->getLat();
        $Object['lng'] = $this->getLng();
        return $Object;
    }

}
