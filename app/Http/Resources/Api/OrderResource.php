<?php

namespace App\Http\Resources\Api;

use App\Models\TimeInterval;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $review = $this->review;
        $Object['id'] = $this->getId();
        $Object['customer_name'] = $this->customer->getName();
        $Object['customer_mobile'] = $this->customer->getMobile();
        $Object['provider_name'] = $this->provider->getName();
        $Object['provider_mobile'] = $this->provider->getMobile();
        $Object['OrderServices'] = OrderServiceResource::collection($this->order_services);
        $Object['price'] = round($this->getPrice(),2);
        $Object['TimeInterval'] = new TimeIntervalResource($this->time_interval);
        $Object['deliver_date'] = ($this->getDeliverDate())?Carbon::parse($this->getDeliverDate())->format('Y-m-d'):null;
        $Object['customer_lat'] = $this->getCustomerLat();
        $Object['customer_lng'] = $this->getCustomerLng();
        $Object['provider_lat'] = $this->getProviderLat();
        $Object['provider_lng'] = $this->getProviderLng();
        $Object['address'] = $this->getAddress();
        $Object['status'] = $this->getStatus().'';
        $Object['is_reviewed'] = ($review)?true:false;
        $Object['Review'] = new ReviewResource($review);
        return $Object;
    }

}
