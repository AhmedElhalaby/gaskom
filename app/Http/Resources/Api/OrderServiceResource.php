<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderServiceResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = app()->getLocale();
        $Object['id'] = $this->getId();
        $Object['name'] = ($lang == 'ar')?$this->service->getNameAr():$this->service->getName();
        $Object['quantity'] = $this->getQuantity();
        $Object['unit_price'] = $this->getUnitPrice();
        $Object['total_price'] = $this->getQuantity() * $this->getUnitPrice();
        return $Object;
    }

}
