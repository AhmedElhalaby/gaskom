<?php

namespace App\Http\Resources\Api;

use App\Models\VendorService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class VendorServiceResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $VendorService = VendorService::where('user_id',auth('api')->user()->getId())->where('service_id',$this->getId())->first();
        $Object['id'] = $this->getId();
        $Object['name'] = (app()->getLocale() == 'ar')?$this->getNameAr():$this->getName();
        $Object['size'] = $this->getSize();
        $Object['image'] = $this->getImage();
        $Object['price'] = ($VendorService)?$VendorService->getPrice():$this->getPrice();
        $Object['is_selected'] = ($VendorService)?true:false;
        return $Object;
    }

}
