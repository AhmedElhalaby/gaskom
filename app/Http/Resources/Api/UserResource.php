<?php

namespace App\Http\Resources\Api;

use App\Helpers\Constant;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Subscription;
use App\Models\VendorService;
use App\Models\VendorSubscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    protected $token;

    /**
     * ExportResource constructor.
     * @param $resource
     * @param array $token
     */
    public function __construct($resource, $token =null)
    {
        $this->token = $token;
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $review_order = Order::where('user_id',$this->id)->where('status',Constant::ORDER_STATUS['Received'])->where('is_reviewed',false)->first();
        $Object['id'] = $this->getId();
        $Object['name'] = $this->getName();
        $Object['mobile'] = $this->getMobile();
        $Object['mobile_verified_at'] = $this->getMobileVerifiedAt();
        $Object['app_locale'] = $this->getAppLocale();
        $Object['lat'] = $this->getLat();
        $Object['lng'] = $this->getLng();
        $Object['address'] = $this->getAddress();
        $Object['type'] = $this->getType();
        $Object['image'] = ($this->getImage())?asset($this->getImage()):null;
        $Object['license_plate'] = $this->getLicensePlate();
        $Object['is_subscription_expire'] = VendorSubscription::where('user_id',$this->id)->whereIn('status',[Constant::VENDOR_SUBSCRIPTION_STATUS['Pending'],Constant::VENDOR_SUBSCRIPTION_STATUS['Approved']])->where('expire_at','>',Carbon::today())->first() ?false:true;
        $Object['service_count'] = count(VendorService::where('user_id',$this->id)->get());
        $Object['rate'] = $this->getRateAvg();
        $Object['notification_count'] = Notification::where('user_id',$this->id)->where('read_at',null)->count();
        $Object['review_order_id'] = $review_order ?$review_order->getId():null;
        $Object['review_provider_name'] = $review_order ?$review_order->provider->getName():null;
        $Object['access_token'] = $this->token;
        $Object['token_type'] = 'Bearer';
        return $Object;
    }

}
