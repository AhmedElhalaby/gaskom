<?php

namespace App\Http\Resources\Api;

use App\Models\Favourite;
use App\Models\Service;
use App\Models\VendorService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProviderResource extends JsonResource
{
    protected $token;

    /**
     * ExportResource constructor.
     * @param $resource
     * @param array $token
     */
    public function __construct($resource, $token =null)
    {
        $this->token = $token;
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Services = Service::whereIn('id',VendorService::where('user_id',$this->getId())->pluck('service_id'))->get();
        $Object['id'] = $this->getId();
        $Object['name'] = $this->getName();
        $Object['mobile'] = $this->getMobile();
        $Object['lat'] = $this->getLat();
        $Object['lng'] = $this->getLng();
        $Object['image'] = ($this->getImage())?asset($this->getImage()):null;
        $Object['license_plate'] = $this->getLicensePlate();
        $Object['Services'] = ServiceResource::collection($Services);
        $Object['is_favourite'] = Favourite::where('user_id',auth()->user()->getId())->where('provider_id',$this->id)->first() ? true : false;
        $Object['rate'] = $this->getRateAvg();
        return $Object;
    }

}
