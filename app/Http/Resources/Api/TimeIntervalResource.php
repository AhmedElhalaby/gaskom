<?php

namespace App\Http\Resources\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TimeIntervalResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = app()->getLocale();
        $Object['id'] = $this->getId();
        $Object['name'] =Carbon::parse($this->getName())->format('H:i A') .' - '. Carbon::parse($this->getNameAr())->format('H:i A');
        return $Object;
    }

}
