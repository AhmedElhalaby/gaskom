<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class VendorSubscriptionResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['name'] = $this->subscription->getName();
        $Object['description'] = $this->subscription->getDescription();
        $Object['price'] = $this->subscription->getPrice();
        $Object['image'] = $this->subscription->getImage();
        $Object['expire_at'] = $this->getExpireAt();
        $Object['status'] = $this->isStatus();
        $Object['is_expire'] = $this->isExpire();
        return $Object;
    }

}
