<?php

namespace App\Http\Resources\Api;

use App\Models\VendorService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = app()->getLocale();
        $Object['id'] = $this->getId();
        $Object['name'] = ($lang == 'ar')?$this->getNameAr():$this->getName();
        $Object['size'] = $this->getSize();
        $Object['image'] = asset($this->getImage());
        $Object['price'] = round($this->getPrice(),2);
        return $Object;
    }

}
