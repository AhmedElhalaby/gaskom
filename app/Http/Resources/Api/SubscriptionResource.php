<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SubscriptionResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = app()->getLocale();
        $Object['id'] = $this->getId();
        $Object['name'] = ($lang == 'ar')?$this->getNameAr():$this->getName();
        $Object['description'] = ($lang == 'ar')?$this->getDescriptionAr():$this->getDescription();
        $Object['price'] = round($this->getPrice(),2);
        $Object['image'] = asset($this->getImage());
        return $Object;
    }

}
