<?php

namespace App\Http\Requests\Api\Subscription;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\UserResource;
use App\Http\Resources\Api\VendorSubscriptionResource;
use App\Models\Subscription;
use App\Models\VendorSubscription;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class SubscribeRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subscription_id' => 'required|exists:subscriptions,id',
            'payment_type' => 'required|in:'.Constant::PAYMENT_TYPE_RULES,
            'payment_document' => 'required_if:payment_type,'.Constant::PAYMENT_TYPE['Bank']
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        if(auth()->user()->type != Constant::USER_TYPE['Vendor']){
            return $this->failJsonResponse([__('messages.you_are_not_a_vendor')]);
        }
        $today = Carbon::today();
        $logged = $this->user();
        $Subscription = Subscription::find($this->subscription_id);
        $VendorSubscription = VendorSubscription::where('user_id',$logged->getId())->where('subscription_id',$this->subscription_id)->first();
        if(!$VendorSubscription){
            $VendorSubscription = new VendorSubscription();
            $VendorSubscription->setUserId($logged->getId());
            $VendorSubscription->setSubscriptionId($this->subscription_id);
            $VendorSubscription->setExpireAt($today->addMonths($Subscription->getMonthsPeriod()));
        }
        switch ($this->payment_type){
            case Constant::PAYMENT_TYPE['Cash']:{
                $VendorSubscription->setPaymentType(Constant::PAYMENT_TYPE['Cash']);
                break;
            }
            case Constant::PAYMENT_TYPE['Bank']:{
                $VendorSubscription->setPaymentType(Constant::PAYMENT_TYPE['Bank']);
                $payment_document = $this->file('payment_document');
                $destination_path = "storage/VendorSubscription/";
                $new_file_name = md5($payment_document->getClientOriginalName().time()).'.'.$payment_document->getClientOriginalExtension();
                $payment_document->move($destination_path, $new_file_name);
                $VendorSubscription->setPaymentDocument($destination_path.$new_file_name);
                break;
            }
        }
        $VendorSubscription->save();
        $VendorSubscription->refresh();
        return $this->successJsonResponse( [__('messages.saved_successfully')], new VendorSubscriptionResource($VendorSubscription),'VendorSubscription');
    }
}
