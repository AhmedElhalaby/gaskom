<?php

namespace App\Http\Requests\Api\Subscription;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\UserResource;
use App\Http\Resources\Api\VendorSubscriptionResource;
use App\Models\VendorSubscription;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class MeRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        if(auth()->user()->type != Constant::USER_TYPE['Vendor']){
            return $this->failJsonResponse([__('messages.you_are_not_a_vendor')]);
        }
        $VendorSubscription = VendorSubscription::where('user_id',$this->user()->getId())->first();
        if($VendorSubscription){
            return $this->successJsonResponse( [], new VendorSubscriptionResource($VendorSubscription),'VendorSubscription');
        }
        return $this->failJsonResponse( [__('messages.wrong_data')]);

    }
}
