<?php

namespace App\Http\Requests\Api\AddressBook;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\AddressBookResource;
use App\Models\AddressBook;
use App\Traits\ResponseTrait;


class UpdateRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_id'=>'required|exists:addresses_book,id',
            'address'=>'required|string',
            'lat'=>'required|string',
            'lng'=>'required|string',
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    public function persist()
    {
        $logged = auth()->user();
        $AddressBook = AddressBook::find($this->address_id);
        $AddressBook->setAddress($this->address);
        $AddressBook->setLat($this->lat);
        $AddressBook->setLng($this->lng);
        $AddressBook->save();
        return $this->successJsonResponse( [__('messages.updated_successful')], new AddressBookResource($AddressBook),'AddressBook');
    }
}
