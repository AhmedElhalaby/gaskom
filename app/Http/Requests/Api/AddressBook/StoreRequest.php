<?php

namespace App\Http\Requests\Api\AddressBook;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\AddressBookResource;
use App\Http\Resources\Api\ProviderResource;
use App\Models\AddressBook;
use App\Models\Favourite;
use App\Traits\ResponseTrait;


class StoreRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'=>'required|string',
            'lat'=>'required|string',
            'lng'=>'required|string',
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    public function persist()
    {
        $logged = auth()->user();
        $AddressBook = new AddressBook();
        $AddressBook->setUserId($logged->getId());
        $AddressBook->setAddress($this->address);
        $AddressBook->setLat($this->lat);
        $AddressBook->setLng($this->lng);
        $AddressBook->save();
        return $this->successJsonResponse( [__('messages.created_successful')], new AddressBookResource($AddressBook),'AddressBook');
    }
}
