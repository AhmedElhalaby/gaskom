<?php

namespace App\Http\Requests\Api\AddressBook;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\AddressBookResource;
use App\Models\AddressBook;
use App\Models\Favourite;
use App\Traits\ResponseTrait;


class IndexRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    public function persist()
    {
        $logged = auth()->user();
        $Objects = AddressBook::where('user_id',$logged->getId())->get();
        return $this->successJsonResponse( [], AddressBookResource::collection($Objects),'AddressBook');
    }
}
