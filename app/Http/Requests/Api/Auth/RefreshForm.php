<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\UserResource;
use App\Master;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;

class RefreshForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_type' => 'required|string',
            'device_token' => 'required|string'
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        $logged = $this->user();
        $logged->device_token = $this->device_token;
        $logged->device_type = $this->device_type;
        $logged->save();
        $logged->token()->revoke();
        $logged->token()->delete();
        $tokenResult = $logged->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        return $this->successJsonResponse( [__('messages.updated_successful')],new UserResource($logged,$tokenResult->accessToken),'User');
    }
}
