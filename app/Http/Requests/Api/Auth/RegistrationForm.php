<?php

namespace App\Http\Requests\Api\Auth;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\UserResource;
use App\Models\VerifyAccounts;
use App\Notifications\VerifyAccount;
use App\Traits\ResponseTrait;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/**
 * @property mixed name
 * @property mixed password
 * @property mixed mobile
 * @property mixed device_token
 * @property mixed device_type
 */
class RegistrationForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'password' => 'required|string|min:6',
            'mobile' => 'required|numeric|unique:users',
            'type' => 'required|in:'.Constant::USER_TYPE_RULES,
            'device_token' => 'string|required_with:device_type',
            'device_type' => 'string|required_with:device_token',
            'image' => 'mimes:jpeg,jpg,png,gif|required_if:type,'.Constant::USER_TYPE['Vendor'],
            'license_plate' => 'required_if:type,'.Constant::USER_TYPE['Vendor'],

        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        $user = new User();
        $user->setName($this->name);
        $user->setPassword($this->password);
        $user->setMobile($this->mobile);
        if ($this->input('device_token')) {
            $user->setDeviceToken($this->device_token);
            $user->setDeviceType($this->device_type);
        }
        if($this->type == Constant::USER_TYPE['Vendor']){
            $user->setLicensePlate($this->license_plate);
            $image = $this->file('image');
            $destination_path = "storage/users/";
            $new_file_name = md5($image->getClientOriginalName().time()).'.'.$image->getClientOriginalExtension();
            $image->move($destination_path, $new_file_name);
            $user->setImage($destination_path.$new_file_name);
        }
        $user->setLat($this->lat);
        $user->setLng($this->lng);
        $user->setAddress(@$this->address);
        $user->setType($this->type);
        $user->save();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $user->refresh();
        $code = rand( 10000 , 99999 );
        $token = Str::random(40).time();
        $VerifyAccounts = VerifyAccounts::updateOrCreate(
            ['mobile' => $this->mobile],
            [
                'mobile' => $this->mobile,
                'code' => $code,
            ]
        );
        $user->notify(
            new VerifyAccount($token,$code)
        );
        return $this->successJsonResponse( [__('messages.saved_successfully')],new UserResource($user,$tokenResult->accessToken),'User');

    }

}
