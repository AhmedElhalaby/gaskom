<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Master;
use App\Models\PasswordReset;
use App\Notifications\PasswordReset as PasswordResetNotification;
use App\Traits\ResponseTrait;
use App\Models\User;

class ForgetPasswordRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => 'required|numeric|exists:users,mobile',
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        $user = User::where('mobile',$this->mobile)->first();
        if($user){
            $code = rand( 10000 , 99999 );
            $passwordReset = PasswordReset::updateOrCreate(
                ['mobile' => $user->mobile],
                [
                    'mobile' => $user->mobile,
                    'code' => $code,
                ]
            );
            $user->notify(
                new PasswordResetNotification($code)
            );
            return $this->successJsonResponse([__('auth.code_sent')] );
        }
        return $this->failJsonResponse([__('messages.object_not_found')]);
    }
}
