<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\OrderResource;
use App\Http\Resources\Api\ProviderResource;
use App\Models\Order;
use App\Models\User;
use App\Models\VendorService;
use App\Traits\ResponseTrait;


class ChangeStatusRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
            'status'=>'required|in:'.Constant::ORDER_STATUS_RULES
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    public function persist()
    {
        $logged = $this->user();
        $Order = (new Order)->find($this->order_id);
        if($logged->getId() != $Order->getProviderId()){
            return $this->failJsonResponse(['messages.wrong_object']);
        }
        switch ($this->status){
            case Constant::ORDER_STATUS['Accepted']:{
                if ($Order->getStatus() != Constant::ORDER_STATUS['Pending']){
                    return $this->failJsonResponse(['messages.wrong_sequence']);
                }
                $Order->setStatus($this->status);
                $Order->save();
                Functions::SendNotification(
                    $Order->customer,
                    __('messages.OrderStatus.Accepted.title',[],'en'),
                    __('messages.OrderStatus.Accepted.message',[],'en'),
                    __('messages.OrderStatus.Accepted.title',[],'ar'),
                    __('messages.OrderStatus.Accepted.message',[],'ar'),
                    $Order->getId(),
                    Constant::NOTIFICATION_TYPE['Order']
                );
                break;
            }
            case Constant::ORDER_STATUS['Rejected']:{
                if ($Order->getStatus() != Constant::ORDER_STATUS['Pending']){
                    return $this->failJsonResponse(['messages.wrong_sequence']);
                }
                $Order->setStatus($this->status);
                $Order->save();
                Functions::SendNotification(
                    $Order->customer,
                    __('messages.OrderStatus.Rejected.title',[],'en'),
                    __('messages.OrderStatus.Rejected.message',[],'en'),
                    __('messages.OrderStatus.Rejected.title',[],'ar'),
                    __('messages.OrderStatus.Rejected.message',[],'ar'),
                    $Order->getId(),
                    Constant::NOTIFICATION_TYPE['Order']
                );
                break;
            }
            case Constant::ORDER_STATUS['Processing']:{
                if ($Order->getStatus() != Constant::ORDER_STATUS['Accepted']){
                    return $this->failJsonResponse(['messages.wrong_sequence']);
                }
                $Order->setStatus($this->status);
                $Order->save();
                Functions::SendNotification(
                    $Order->customer,
                    __('messages.OrderStatus.Processing.title',[],'en'),
                    __('messages.OrderStatus.Processing.message',[],'en'),
                    __('messages.OrderStatus.Processing.title',[],'ar'),
                    __('messages.OrderStatus.Processing.message',[],'ar'),
                    $Order->getId(),
                    Constant::NOTIFICATION_TYPE['Order']
                );
                break;
            }
            case Constant::ORDER_STATUS['Delivered']:{
                if ($Order->getStatus() != Constant::ORDER_STATUS['Processing']){
                    return $this->failJsonResponse(['messages.wrong_sequence']);
                }
                $Order->setStatus($this->status);
                $Order->save();
                Functions::SendNotification(
                    $Order->customer,
                    __('messages.OrderStatus.Delivered.title',[],'en'),
                    __('messages.OrderStatus.Delivered.message',[],'en'),
                    __('messages.OrderStatus.Delivered.title',[],'ar'),
                    __('messages.OrderStatus.Delivered.message',[],'ar'),
                    $Order->getId(),
                    Constant::NOTIFICATION_TYPE['Order']
                );
                break;
            }
            case Constant::ORDER_STATUS['Received']:{
                if ($Order->getStatus() != Constant::ORDER_STATUS['Delivered']){
                    return $this->failJsonResponse(['messages.wrong_sequence']);
                }
                $Order->setStatus($this->status);
                $Order->save();
                Functions::SendNotification(
                    $Order->customer,
                    __('messages.OrderStatus.Received.title',[],'en'),
                    __('messages.OrderStatus.Received.message',[],'en'),
                    __('messages.OrderStatus.Received.title',[],'ar'),
                    __('messages.OrderStatus.Received.message',[],'ar'),
                    $Order->getId(),
                    Constant::NOTIFICATION_TYPE['Order']
                );
                break;
            }
        }
        return $this->successJsonResponse( [__('messages.updated_successful')], new OrderResource($Order),'Order');
    }
}
