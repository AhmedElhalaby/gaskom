<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\ProviderResource;
use App\Models\User;
use App\Models\VendorService;
use App\Models\VendorSubscription;
use App\Traits\ResponseTrait;


class ProviderRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'services'=>'array',
            'services.*'=>'exists:services,id'
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    public function persist()
    {

        $Providers = new User();
        $Providers = $Providers->where('is_active',true);
        $Subscription = VendorSubscription::where('expire_at','>',now())->where('status',Constant::VENDOR_SUBSCRIPTION_STATUS['Approved'])->pluck('user_id');
        if($this->filled('services')){
            $ProvidersId = VendorService::whereIn('service_id',$this->services)->whereIn('user_id',$Subscription)->pluck('user_id');
            $Providers = $Providers->whereIn('id',$ProvidersId);
        }else{
            $ProvidersId = VendorService::whereIn('user_id',$Subscription)->pluck('user_id');
            $Providers = $Providers->whereIn('id',$ProvidersId);
        }
        $Providers = $Providers->get();
        return $this->successJsonResponse( [], ProviderResource::collection($Providers),'Providers');
    }
}
