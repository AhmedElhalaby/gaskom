<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\OrderResource;
use App\Http\Resources\Api\ProviderResource;
use App\Models\Order;
use App\Models\User;
use App\Models\VendorService;
use App\Traits\ResponseTrait;


class CancelRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    public function persist()
    {
        $logged = $this->user();
        $Order = (new Order)->find($this->order_id);
        if($logged->getId() != $Order->getUserId()){
            return $this->failJsonResponse(['messages.wrong_object']);
        }
        if($Order->getStatus() != Constant::ORDER_STATUS['Pending']){
            return $this->failJsonResponse(['messages.wrong_sequence']);
        }
        $Order->setStatus(Constant::ORDER_STATUS['Canceled']);
        $Order->save();

        Functions::SendNotification(
            $Order->provider,
            __('messages.OrderStatus.Canceled.title',[],'en'),
            __('messages.OrderStatus.Canceled.message',[],'en'),
            __('messages.OrderStatus.Canceled.title',[],'ar'),
            __('messages.OrderStatus.Canceled.message',[],'ar'),
            $Order->getId(),
            Constant::NOTIFICATION_TYPE['Order']
        );
        return $this->successJsonResponse( [__('messages.saved_successfully')], new OrderResource($Order),'Order');
    }
}
