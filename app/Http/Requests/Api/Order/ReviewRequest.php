<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\OrderResource;
use App\Http\Resources\Api\ProviderResource;
use App\Models\Order;
use App\Models\Review;
use App\Models\User;
use App\Models\VendorService;
use App\Traits\ResponseTrait;


class ReviewRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
            'rate'=>'required|numeric|in:1,2,3,4,5',
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    public function persist()
    {
        $logged = $this->user();
        $Order = Order::find($this->order_id);
        $Review = Review::updateOrCreate(
            ['order_id' => $this->order_id,'user_id'=>$logged->getId(),'provider_id'=>$Order->getProviderId()],
            [
                'rate' => $this->rate,
                'review' => @$this->review,
            ]
        );
        $Order->setIsReviewed(true);
        $Order->save();
        return $this->successJsonResponse( [__('messages.rated_successfully')], new OrderResource($Order),'Order');
    }
}
