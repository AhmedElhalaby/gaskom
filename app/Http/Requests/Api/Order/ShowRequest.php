<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\OrderResource;
use App\Models\Order;
use App\Traits\ResponseTrait;


class ShowRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'integer|exists:orders,id',
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        $Orders = (new Order)->find($this->order_id);
        return $this->successJsonResponse( [], new OrderResource($Orders),'Order');
    }
}
