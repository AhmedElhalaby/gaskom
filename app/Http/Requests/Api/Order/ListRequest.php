<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\OrderResource;
use App\Models\Order;
use App\Traits\ResponseTrait;


class ListRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'integer|in:'.Constant::ORDER_STATUS_RULES,
            'is_completed' => 'boolean'
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        $logged = $this->user();
        $Orders = new Order();
        if ($this->filled('status')){
            $Orders = $Orders->where('status',$this->status);
        }
        if($logged->type == Constant::USER_TYPE['Vendor']){
            $Orders = $Orders->where('provider_id',$logged->getId());
        }
        if ($logged->type == Constant::USER_TYPE['Customer']){
            $Orders = $Orders->where('user_id',$logged->getId());
        }
        if($this->filled('is_completed')){
            if ($this->is_completed){
                $Orders = $Orders->whereIn('status',[Constant::ORDER_STATUS['Received'],Constant::ORDER_STATUS['Canceled'],Constant::ORDER_STATUS['Rejected']]);
            }else{
                $Orders = $Orders->whereNotIn('status',[Constant::ORDER_STATUS['Received'],Constant::ORDER_STATUS['Canceled'],Constant::ORDER_STATUS['Rejected']]);
            }
        }
        $Orders = $Orders->orderBy('created_at','desc');
        $Orders = $Orders->paginate($this->per_page ?? 10);
        return $this->successJsonResponse( [], OrderResource::collection($Orders),'Orders',$Orders);
    }
}
