<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\OrderResource;
use App\Http\Resources\Api\ProviderResource;
use App\Models\Order;
use App\Models\OrderService;
use App\Models\Service;
use App\Models\User;
use App\Models\VendorService;
use App\Traits\ResponseTrait;


class StoreRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider_id'=>'required|exists:users,id',
            'services'=>'required|array',
            'services.*.service_id'=>'required|exists:services,id',
            'services.*.quantity'=>'required|numeric',
            'time_interval_id'=>'required|exists:time_intervals,id',
            'customer_lat'=>'required',
            'customer_lng'=>'required',
            'address'=>'required',
            'deliver_date'=>'required',
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    public function persist()
    {
        $price = 0;
        $logged = auth()->user();
        $Provider = (new \App\Models\User)->find($this->provider_id);
        $Order = new Order();
        $Order->setUserId($logged->getId());
        $Order->setProviderId($this->provider_id);
        $Order->setTimeIntervalId($this->time_interval_id);
        $Order->setCustomerLat($this->customer_lat);
        $Order->setCustomerLng($this->customer_lng);
        $Order->setProviderLat($Provider->getLat());
        $Order->setProviderLng($Provider->getLng());
        $Order->setAddress($this->address);
        $Order->setDeliverDate($this->deliver_date);
        $Order->setPrice($price);
        $Order->save();
        $Order->refresh();
        foreach ($this->services as $service){
            $VendorService = VendorService::where('user_id',$this->provider_id)->where('service_id',$service['service_id'])->first();
            $Service = Service::find($service['service_id']);
            $OrderService = new OrderService();
            $OrderService->setServiceId($service['service_id']);
            $OrderService->setOrderId($Order->getId());
            $OrderService->setQuantity($service['quantity']);
            $OrderService->setUnitPrice($VendorService->price ?? $Service->price);
            $OrderService->save();
            $price += $OrderService->getQuantity() *$OrderService->getUnitPrice();
        }
        $Order->setPrice($price);
        $Order->save();
        Functions::SendNotification(
            $Provider,
            __('messages.OrderStatus.New.title',[],'en'),
            __('messages.OrderStatus.New.message',[],'en'),
            __('messages.OrderStatus.New.title',[],'ar'),
            __('messages.OrderStatus.New.message',[],'ar'),
            $Order->getId(),
            Constant::NOTIFICATION_TYPE['Order']
        );
        return $this->successJsonResponse( [__('messages.created_successful')], new OrderResource($Order),'Order');
    }
}
