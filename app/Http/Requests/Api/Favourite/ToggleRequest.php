<?php

namespace App\Http\Requests\Api\Favourite;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\ProviderResource;
use App\Models\Favourite;
use App\Traits\ResponseTrait;


class ToggleRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider_id'=>'required|exists:users,id',
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    public function persist()
    {
        $logged = auth()->user();
        $Favourite = Favourite::where('user_id',$logged->id)->where('provider_id',$this->provider_id)->first();
        $Provider = (new \App\Models\User)->find($this->provider_id);
        if($Favourite){
            $Favourite->delete();
        }else{
            $Favourite = new Favourite();
            $Favourite->setUserId(auth()->user()->getId());
            $Favourite->setProviderId($this->provider_id);
            $Favourite->save();
        }
        return $this->successJsonResponse( [__('messages.saved_successfully')], new ProviderResource($Provider),'Provider');
    }
}
