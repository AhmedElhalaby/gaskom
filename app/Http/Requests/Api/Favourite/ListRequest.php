<?php

namespace App\Http\Requests\Api\Favourite;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\ProviderResource;
use App\Models\Favourite;
use App\Traits\ResponseTrait;


class ListRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function attributes()
    {
        return [
        ];
    }
    public function persist()
    {
        $logged = auth()->user();
        $ProvidersId = Favourite::where('user_id',$logged->id)->pluck('provider_id');
        $Providers = (new \App\Models\User)->whereIn('id',$ProvidersId)->paginate($this->per_page ?? 10);
        return $this->successJsonResponse( [], ProviderResource::collection($Providers),'Providers');
    }
}
