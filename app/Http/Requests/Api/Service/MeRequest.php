<?php

namespace App\Http\Requests\Api\Service;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\UserResource;
use App\Http\Resources\Api\VendorServiceResource;
use App\Http\Resources\Api\VendorSubscriptionResource;
use App\Models\Service;
use App\Models\VendorService;
use App\Models\VendorSubscription;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class MeRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        if(auth('api')->user()->type != Constant::USER_TYPE['Vendor']){
            return $this->failJsonResponse([__('messages.you_are_not_a_vendor')]);
        }
        $Services = Service::all();
        if($Services){
            return $this->successJsonResponse( [], VendorServiceResource::collection($Services),'VendorServices');
        }
        return $this->failJsonResponse([__('messages.wrong_data')]);
    }
}
