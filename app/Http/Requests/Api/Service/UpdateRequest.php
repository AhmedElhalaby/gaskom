<?php

namespace App\Http\Requests\Api\Service;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\UserResource;
use App\Http\Resources\Api\VendorServiceResource;
use App\Http\Resources\Api\VendorSubscriptionResource;
use App\Models\Service;
use App\Models\Subscription;
use App\Models\VendorService;
use App\Models\VendorSubscription;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class UpdateRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'services' => 'required|array',
            'services.*.service_id' => 'required|exists:services,id',
            'services.*.price' => 'required',
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        $logged = $this->user();
        if($logged->type != Constant::USER_TYPE['Vendor']){
            return $this->failJsonResponse([__('messages.you_are_not_a_vendor')]);
        }
        VendorService::where('user_id',$logged->getId())->delete();
        foreach ($this->services as $service){
            $VendorService = new VendorService();
            $VendorService->setServiceId($service['service_id']);
            $VendorService->setUserId($logged->getId());
            $VendorService->setPrice($service['price']);
            $VendorService->save();
        }
        return $this->successJsonResponse( [__('messages.updated_successful')], VendorServiceResource::collection(Service::all()),'VendorServices');
    }
}
