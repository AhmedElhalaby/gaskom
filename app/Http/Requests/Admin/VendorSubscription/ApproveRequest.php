<?php

namespace App\Http\Requests\Admin\VendorSubscription;

use App\Helpers\Constant;
use App\Models\Admin;
use App\Models\VendorSubscription;
use Illuminate\Foundation\Http\FormRequest;

class ApproveRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($redirect,$id){
        $Object = VendorSubscription::find($id);
        if(!$Object)
            return redirect($redirect)->withErrors(__('admin.messages.wrong_data'));
        $Object->setStatus(Constant::VENDOR_SUBSCRIPTION_STATUS['Approved']);
        $Object->save();
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
