<?php

namespace App\Http\Requests\Admin\Admin;

use App\Master;
use App\Models\Admin;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Objects = new Admin();
        $Columns = Admin::$Columns;
        $Links = Admin::$Links;
        if($this->has('q')){
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%')->orwhere('email','LIKE','%'.$this->q.'%');
        }
        if($this->has('name')){
            $Objects = $Objects->where('name','LIKE','%'.$this->name.'%');
        }
        if($this->has('email')){
            $Objects = $Objects->where('email','LIKE','%'.$this->email.'%');
        }
        if($this->has('is_active')){
            $Objects = $Objects->where('is_active','LIKE','%'.$this->is_active.'%');
        }
        if($this->has('order_by') && $this->has('order_type')){
            $Objects = $Objects->orderBy($this->order_by,$this->order_type);
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects','Columns','Links'))->with($params);
    }
}
