<?php


namespace App\Helpers;


class Constant
{
    const USER_TYPE=[
        'Customer'=>1,
        'Vendor'=>2
    ];
    const USER_TYPE_RULES='1,2';
    const PAYMENT_TYPE=[
        'Cash'=>1,
        'Bank'=>2
    ];
    const VENDOR_SUBSCRIPTION_STATUS=[
        'Pending'=>0,
        'Approved'=>1,
        'Declined'=>2
    ];
    const PAYMENT_TYPE_RULES='1,2,3';
    const ORDER_STATUS=[
        'Pending'=>0,
        'Canceled'=>1,
        'Accepted'=>2,
        'Rejected'=>3,
        'Processing'=>4,
        'Delivered'=>5,
        'Received'=>6,
    ];
    const ORDER_STATUS_RULES='0,1,2,3,4,5,6';

    const NOTIFICATION_TYPE=[
        'General'=>0,
        'Order'=>1,
    ];
}
