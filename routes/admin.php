<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Admin" middleware group. Enjoy building your Admin!
|
*/
Route::get('app/lang', 'HomeController@lang');


/*
|--------------------------------------------------------------------------
| Admin Auth
|--------------------------------------------------------------------------
| Here is where admin auth routes exists for login and log out
*/
Route::group([
    'namespace'  => 'Auth',
], function() {
    Route::get('login', ['uses' => 'LoginController@showLoginForm','as'=>'admin.login']);
    Route::post('login', ['uses' => 'LoginController@login']);
    Route::group([
        'middleware' => 'App\Http\Middleware\RedirectIfAuthenticatedAdmin',
    ], function() {
        Route::post('logout', ['uses' => 'LoginController@logout','as'=>'admin.logout']);
    });
});
/*
|--------------------------------------------------------------------------
| Admin After login in
|--------------------------------------------------------------------------
| Here is where admin panel routes exists after login in
*/
Route::group([
    'middleware'  => ['App\Http\Middleware\RedirectIfAuthenticatedAdmin'],
], function() {
    Route::get('/', 'HomeController@index');
    Route::post('notification/send', 'HomeController@general_notification');

    /*
    |--------------------------------------------------------------------------
    | Admin > App Management
    |--------------------------------------------------------------------------
    | Here is where App Management routes
    */

    Route::group([
        'prefix'=>'app_managements',
        'namespace'=>'AppManagement',
    ],function () {
        Route::group([
            'prefix'=>'admins'
        ],function () {
            Route::get('/','AdminController@index');
            Route::get('/create','AdminController@create');
            Route::post('/','AdminController@store');
            Route::get('/{admin}','AdminController@show');
            Route::get('/{admin}/edit','AdminController@edit');
            Route::put('/{admin}','AdminController@update');
            Route::delete('/{admin}','AdminController@destroy');
            Route::patch('/update/password',  'AdminController@updatePassword');
            Route::get('/option/export','AdminController@export');
            Route::get('/{id}/activation','AdminController@activation');
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Admin > App Data
    |--------------------------------------------------------------------------
    | Here is where App Data routes
    */

    Route::group([
        'prefix'=>'app_data',
        'namespace'=>'AppData',
    ],function () {
        Route::group([
            'prefix'=>'services'
        ],function () {
            Route::get('/','ServiceController@index');
            Route::get('/create','ServiceController@create');
            Route::post('/','ServiceController@store');
            Route::get('/{service}','ServiceController@show');
            Route::get('/{service}/edit','ServiceController@edit');
            Route::put('/{service}','ServiceController@update');
            Route::delete('/{service}','ServiceController@destroy');
            Route::get('/option/export','ServiceController@export');
        });
        Route::group([
            'prefix'=>'subscriptions'
        ],function () {
            Route::get('/','SubscriptionController@index');
            Route::get('/create','SubscriptionController@create');
            Route::post('/','SubscriptionController@store');
            Route::get('/{subscription}','SubscriptionController@show');
            Route::get('/{subscription}/edit','SubscriptionController@edit');
            Route::put('/{subscription}','SubscriptionController@update');
            Route::delete('/{subscription}','SubscriptionController@destroy');
            Route::get('/option/export','SubscriptionController@export');
        });
        Route::group([
            'prefix'=>'time_intervals'
        ],function () {
            Route::get('/','TimeIntervalController@index');
            Route::get('/create','TimeIntervalController@create');
            Route::post('/','TimeIntervalController@store');
            Route::get('/{time_interval}','TimeIntervalController@show');
            Route::get('/{time_interval}/edit','TimeIntervalController@edit');
            Route::put('/{time_interval}','TimeIntervalController@update');
            Route::delete('/{time_interval}','TimeIntervalController@destroy');
            Route::get('/option/export','TimeIntervalController@export');
        });
        Route::group([
            'prefix'=>'bank_accounts'
        ],function () {
            Route::get('/','BankAccountController@index');
            Route::get('/create','BankAccountController@create');
            Route::post('/','BankAccountController@store');
            Route::get('/{bank_account}','BankAccountController@show');
            Route::get('/{bank_account}/edit','BankAccountController@edit');
            Route::put('/{bank_account}','BankAccountController@update');
            Route::delete('/{bank_account}','BankAccountController@destroy');
            Route::get('/option/export','BankAccountController@export');
        });
        Route::group([
            'prefix'=>'settings'
        ],function () {
            Route::get('/','SettingController@index');
            Route::get('/{setting}/edit','SettingController@edit');
            Route::put('/{setting}','SettingController@update');
        });
    });

    Route::group([
        'prefix'=>'vendor_management',
        'namespace'=>'VendorManagement',
    ],function () {
        Route::group([
            'prefix'=>'subscriptions'
        ],function () {
            Route::get('/','VendorSubscriptionController@index');
            Route::get('/{vendor_subscription}/approve','VendorSubscriptionController@approve');
            Route::get('/{vendor_subscriptions}/decline','VendorSubscriptionController@decline');
        });
        Route::group([
            'prefix'=>'vendors'
        ],function () {
            Route::get('/','VendorController@index');
            Route::get('/{id}','VendorController@show');
            Route::patch('/update/password',  'VendorController@updatePassword');
            Route::get('/option/export','VendorController@export');
            Route::get('/{id}/activation','VendorController@activation');
            Route::delete('/{vendor}','VendorController@destroy');

        });
    });
    Route::group([
        'prefix'=>'order_management',
        'namespace'=>'OrderManagement',
    ],function () {
        Route::group([
            'prefix'=>'orders'
        ],function () {
            Route::get('/','OrderController@index');
        });
    });
    Route::group([
        'prefix'=>'customer_management',
        'namespace'=>'CustomerManagement',
    ],function () {
        Route::group([
            'prefix'=>'customers'
        ],function () {
            Route::get('/','CustomerController@index');
            Route::get('/create','CustomerController@create');
            Route::post('/','CustomerController@store');
            Route::get('/{admin}','CustomerController@show');
            Route::get('/{admin}/edit','CustomerController@edit');
            Route::put('/{admin}','CustomerController@update');
            Route::delete('/{admin}','CustomerController@destroy');
            Route::patch('/update/password',  'CustomerController@updatePassword');
            Route::get('/option/export','CustomerController@export');
            Route::get('/{id}/activation','CustomerController@activation');
        });
    });
});
