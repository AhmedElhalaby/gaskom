<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login','AuthController@login');
    Route::post('signup','AuthController@register');
    Route::post('forget_password','AuthController@forget_password');
    Route::post('reset_password','AuthController@reset_password');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('me','AuthController@show');
        Route::post('refresh','AuthController@refresh');
        Route::post('update','AuthController@update');
        Route::get('resend_verify', 'AuthController@resend_verify');
        Route::post('verify', 'AuthController@verify');
        Route::post('change_password','AuthController@change_password');
        Route::post('logout','AuthController@logout');
    });
});
Route::group([
    'middleware' => 'auth:api'
], function() {
    Route::group([
        'prefix' => 'notifications',
    ], function() {
        Route::get('/', 'NotificationController@index');
        Route::post('/read', 'NotificationController@read');
        Route::post('/read/all', 'NotificationController@read_all');
    });
    Route::group([
        'prefix' => 'subscriptions',
    ], function () {
        Route::get('me','SubscriptionController@me');
        Route::post('subscribe','SubscriptionController@subscribe');
    });
    Route::group([
        'prefix' => 'services',
    ], function () {
        Route::get('me','ServiceController@me');
        Route::post('update','ServiceController@update');
    });
    Route::group([
        'prefix' => 'orders',
    ], function () {
        Route::get('providers','OrderController@providers');
        Route::get('list','OrderController@list');
        Route::get('show','OrderController@show');
        Route::post('cancel','OrderController@cancel');
        Route::post('store','OrderController@store');
        Route::post('change_status','OrderController@change_status');
        Route::post('review','OrderController@review');
        Route::post('toggle_favourite','OrderController@toggle_favourite');
    });

    Route::group([
        'prefix' => 'favourites',
    ], function () {
        Route::get('list','FavouriteController@list');
        Route::post('toggle','FavouriteController@toggle');
    });
    Route::group([
        'prefix' => 'addresses_book',
    ], function () {
        Route::get('/','AddressBookController@index');
        Route::get('/show','AddressBookController@show');
        Route::post('/','AddressBookController@store');
        Route::put('/','AddressBookController@update');
        Route::delete('/','AddressBookController@destroy');
    });
});
Route::get('install','HomeController@install');

