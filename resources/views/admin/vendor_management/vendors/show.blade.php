@extends('AhmedPanel.crud.main')
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">{{__('admin.show')}} {{__(('crud.'.$lang.'.crud_the_name'))}}</h4>
                </div>
                <div class="card-content table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                            <th></th>
                            <th>{{__('crud.Service.crud_the_name')}}</th>
                            <th>{{__('crud.Service.price')}}</th>
                        </thead>
                        <tbody>
                            @foreach(\App\Models\VendorService::where('user_id',$Object->getId())->get() as $service)
                                <tr>
                                    <td>
                                        <span><img src="{{asset($service->image)}}" class="thumbnail" alt="" style="width: 50px;height: 50px"></span>
                                    </td>
                                    <td>{{(app()->getLocale() == 'ar')?$service->service->getNameAr():$service->service->getName()}}</td>
                                    <td>{{$service->service->getPrice()}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-profile">
                <div class="card-avatar">
                    <a href="">
                        <img class="img" alt="" src="{{asset(($Object->image)?$Object->image:'assets/img/faces/user.png')}}">
                    </a>
                </div>
                <div class="content">
                    <h4 class="card-title">{{$Object->name}}</h4>
                    <h6 class="category text-gray" data-toggle="tooltip" data-placement="bottom" title="{{__('crud.Vendor.mobile')}}"><i class="fa fa-phone"> </i> {{$Object->mobile}}</h6>
                    <br>
                    <p class="text-left">
                        <span class="col-md-8" data-toggle="tooltip" data-placement="bottom" title="{{__('crud.Vendor.address')}}">
                            <i class="fa fa-map-marker"></i> {{$Object->address}}
                        </span>
                        <span class="col-md-4" data-toggle="tooltip" data-placement="bottom" title="{{__('crud.Vendor.license_plate')}}">
                            <i class="fa fa-id-card-o"></i> {{$Object->license_plate}}
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
