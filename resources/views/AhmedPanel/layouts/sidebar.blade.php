<li class="nav-item @if(url()->current() == url('admin')) active @endif ">
    <a href="{{url('admin')}}" class="nav-link">
        <i class="material-icons">dashboard</i>
        <p>{{__('admin.sidebar.home')}}</p>
    </a>
</li>
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_managements" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_managements')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_managements'))===0) in @endif" id="app_managements" @if(strpos(url()->current() , url('admin/app_managements'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_managements/admins'))===0) active @endif">
                <a href="{{url('admin/app_managements/admins')}}" class="nav-link">
                    <i class="material-icons">group</i>
                    <p>{{__('admin.sidebar.admins')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>


<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_data" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_data')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_data'))===0) in @endif" id="app_data" @if(strpos(url()->current() , url('admin/app_data'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/services'))===0) active @endif">
                <a href="{{url('admin/app_data/services')}}" class="nav-link">
                    <i class="material-icons">category</i>
                    <p>{{__('admin.sidebar.services')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/subscriptions'))===0) active @endif">
                <a href="{{url('admin/app_data/subscriptions')}}" class="nav-link">
                    <i class="material-icons">description</i>
                    <p>{{__('admin.sidebar.subscriptions')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/time_intervals'))===0) active @endif">
                <a href="{{url('admin/app_data/time_intervals')}}" class="nav-link">
                    <i class="material-icons">access_time</i>
                    <p>{{__('admin.sidebar.time_intervals')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/settings'))===0) active @endif">
                <a href="{{url('admin/app_data/settings')}}" class="nav-link">
                    <i class="material-icons">settings</i>
                    <p>{{__('admin.sidebar.settings')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/bank_accounts'))===0) active @endif">
                <a href="{{url('admin/app_data/bank_accounts')}}" class="nav-link">
                    <i class="material-icons">payments</i>
                    <p>{{__('admin.sidebar.bank_accounts')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>



<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#vendor_management" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.vendor_management')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/vendor_management'))===0) in @endif" id="vendor_management" @if(strpos(url()->current() , url('admin/vendor_management'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/vendor_management/subscriptions'))===0) active @endif">
                <a href="{{url('admin/vendor_management/subscriptions')}}" class="nav-link">
                    <i class="material-icons">category</i>
                    <p>{{__('admin.sidebar.subscriptions')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/vendor_management/vendors'))===0) active @endif">
                <a href="{{url('admin/vendor_management/vendors')}}" class="nav-link">
                    <i class="material-icons">supervisor_account</i>
                    <p>{{__('admin.sidebar.vendors')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#order_management" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.order_management')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/order_management'))===0) in @endif" id="order_management" @if(strpos(url()->current() , url('admin/order_management'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/order_management/orders'))===0) active @endif">
                <a href="{{url('admin/order_management/orders')}}" class="nav-link">
                    <i class="material-icons">sort</i>
                    <p>{{__('admin.sidebar.orders')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>


<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#customer_management" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.customer_management')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/customer_management'))===0) in @endif" id="customer_management" @if(strpos(url()->current() , url('admin/customer_management'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/customer_management/customers'))===0) active @endif">
                <a href="{{url('admin/customer_management/customers')}}" class="nav-link">
                    <i class="material-icons">people</i>
                    <p>{{__('admin.sidebar.customers')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>

