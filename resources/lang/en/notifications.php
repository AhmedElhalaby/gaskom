<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'expectation_right' => [
        'title' => 'Congratulations',
        'message' => 'Your Expectations was right ! you got two points'
    ],
    'expectation_right_team' => [
        'title' => 'Good Job',
        'message' => 'You have expecting the winner team ! you got one point'
    ],
];
